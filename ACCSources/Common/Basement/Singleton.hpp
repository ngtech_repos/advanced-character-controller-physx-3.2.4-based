/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#ifndef __ACC_SINGLETON_HPP__
#define __ACC_SINGLETON_HPP__

#include "ACCSources/Common/Basement/Utility.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

template < typename T >
class Singleton
	:	private NonCopyable
{

public: 

	static T& getSingletonRef();
	static T* getSingleton();
	static void destroy();

protected: 

	explicit Singleton()
	{ 
		Assert( Singleton::ms_Instance == 0 ); 
		Singleton::ms_Instance = static_cast< T* >(this); 
	}
	virtual ~Singleton()
	{ 
		Singleton::ms_Instance = 0; 
	}

private: 

	static T * createInstance();
	static void scheduleForDestruction( void (*)( ) );
	static void destroyInstance( T* );

private: 

	static T * ms_Instance;

};  //class Singleton


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
typename T& Singleton< T >::getSingletonRef()
{
	if ( Singleton::ms_Instance == 0 )
	{
		Singleton::ms_Instance = Singleton::createInstance();
		scheduleForDestruction( Singleton::destroy );
	}
	return *( Singleton::ms_Instance );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
typename T* Singleton< T >::getSingleton()
{
	if ( Singleton::ms_Instance == 0 )
	{
		Singleton::ms_Instance = createInstance();
		scheduleForDestruction( Singleton::destroy );
	}
	return Singleton::ms_Instance;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
void
Singleton< T >::destroy()
{
	if ( Singleton::ms_Instance != 0 ) 
	{
		destroyInstance(Singleton::ms_Instance);
		Singleton::ms_Instance = 0;
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
typename T* Singleton< T >::createInstance()
{
	return new T();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
void
Singleton< T >::scheduleForDestruction( void ( * pFun )( ) )
{
	atexit( pFun );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
void
Singleton< T >::destroyInstance( T* p )
{
	delete p;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
typename T* Singleton< T >::ms_Instance = 0;


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ACC_SINGLETON_HPP__
