/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#ifndef __ACC_COSINE_INTERPOLATION_CURVE_HPP__
#define __ACC_COSINE_INTERPOLATION_CURVE_HPP__

#include "ACCSources/Common/Basement/Utility.hpp"
#include <map>

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#define BASEMENT_MapType			std::map< Type, Type >
#define BASEMENT_MapIterator		BASEMENT_MapType::iterator
#define BASEMENT_ConstMapIterator	BASEMENT_MapType::const_iterator


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename Type >
class CosineInterpolationCurve
{
public:

	CosineInterpolationCurve()
		:	mMinValue( 0 )
		,	mMaxValue( 0 )
	{}

	bool isEmpty() const
	{
		return mMap.empty();
	}

	void clear() { mMap.clear(); }

	void insert( Type index, Type value)
	{
		if ( mMap.empty() )
		{
			mMinValue = index;
			mMaxValue = index;
		}
		else
		{
			mMinValue = std::min< Type >( mMinValue, index );
			mMaxValue = std::max< Type >( mMaxValue, index );
		}
		
		mMap [ index ] = value;
	}

	Type getValue( Type index ) const
	{
		if ( mMap.empty() )
			throw std::exception( "CosineInterpolationCurve::getValue" );
	
		BASEMENT_ConstMapIterator lower = mMap.begin();
		if ( index <= mMinValue )
			return lower->second;

		BASEMENT_ConstMapIterator upper = mMap.end();
		--upper;

		if ( index >= mMaxValue )
			return upper->second;

		upper = mMap.lower_bound( index );
		if ( upper == lower )
			return ( upper->second );

		lower = upper;
		-- lower;

		Type w1 = index - lower->first;

		return cosineInterpolation(
				lower->second // value
			,	upper->second // value
			,	w1 / ( upper->first - lower->first ) // interval value 0..1
		);
	} //  getValue

	Type mMinValue;
	Type mMaxValue;
	BASEMENT_MapType mMap;

}; // class CosineInterpolationCurve


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#undef BASEMENT_MapType
#undef BASEMENT_MapIterator
#undef BASEMENT_ConstMapIterator


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ACC_COSINE_INTERPOLATION_CURVE_HPP__
