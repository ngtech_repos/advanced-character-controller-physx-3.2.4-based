/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#ifndef __ACC_INERT_VALUE_HPP__
#define __ACC_INERT_VALUE_HPP__

#include "ACCSources/Common/Basement/Utility.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename Type >
class InertValue
	:	NonCopyable
{

public:

	InertValue(
			const Type fixedTimeStep
		,	const Type goalValue
		,	const Type stepPerSecond
		,	const Type currentValue = 0
	)
		:	mFrameSimulationTime( fixedTimeStep )
		,	mCurrentRestfloatTime( 0 )
	{
		set(
				goalValue
			,	stepPerSecond
		);

		mCurrentValue = currentValue;
	}

	InertValue( const Type fixedTimeStep )
		:	mFrameSimulationTime( fixedTimeStep )
		,	mCurrentRestfloatTime( 0 )
	{
		reset( true );
	}

	// this methods should be used only
	// if you are sure this is hardly needed
	// no controls or asserts could be done in this method
	void setCurrentValue( const Type& currentValue )
	{
		mCurrentValue = currentValue;
	}

	void reset( bool shouldResetCurrentValue = false )
	{
		if( shouldResetCurrentValue )
			mCurrentValue = 0;

		mCurrentRestfloatTime = 0;
		mIsWorking = false;
	}


	void set(
			const Type goalValue
		,	const Type stepPerSecond
	)
	{
		Assert( stepPerSecond > 0 );
	
		mGoalValue = goalValue;
		mStepPerSecond = stepPerSecond;

		mCurrentStepping = fabs( mGoalValue - mCurrentValue );
		mIsWorking = true;
	}

	Type operator * () const
	{
		return mCurrentValue;
	}


	void update( float timeStep )
	{
		if ( ! mIsWorking )
			return;

		calculateConvergence();
		mCurrentRestfloatTime += timeStep;
		while ( mFrameSimulationTime <= mCurrentRestfloatTime )
		{			
			updateCurrentValue();
			mCurrentRestfloatTime -= mFrameSimulationTime; 
		}
	
	} // update

private:

	void updateCurrentValue() // uses fixed TimeStep!
	{
 		if ( mCurrentValue > mGoalValue )
 		{	
 		 	mCurrentValue -= fabs( mCurrentValue - mGoalValue ) * mStepPerSecond * mFrameSimulationTime * mConvergenceSpeedMultiplier;
 
 			if ( mCurrentValue < mGoalValue )
 				mCurrentValue = mGoalValue;
 		}
 		else if ( mCurrentValue < mGoalValue )
 		{
 	 		mCurrentValue += fabs( mCurrentValue - mGoalValue ) * mStepPerSecond * mFrameSimulationTime * mConvergenceSpeedMultiplier;
 
 			if ( mCurrentValue > mGoalValue )
 				mCurrentValue = mGoalValue;
 		}	
	}

	Type getDistanceDiameterRatio() const
	{
		Type diameter;
		if ( mCurrentValue * mGoalValue >= 0.0 )
		{
			Type current = fabs( mCurrentValue );
			Type goal = fabs( mGoalValue );

			if ( goal > current )
				diameter = goal;
			else
				diameter = current;
		}
		else
		{
			if ( mCurrentValue < 0.0 )
				diameter = - mCurrentValue + mGoalValue;
			else
				diameter = mCurrentValue - mGoalValue;
		}

		Type distance =fabs( mCurrentValue - mGoalValue );
		if ( distance < 0.001 )
			return 1;
		else
			return diameter / fabs( mCurrentValue - mGoalValue );
	}

	void calculateConvergence()
	{
		mConvergenceSpeedMultiplier = 1;//getDistanceDiameterRatio();
	}

private:

	bool mIsWorking;

	const Type mFrameSimulationTime;
	Type mCurrentRestfloatTime;

	Type mCurrentStepping;
	Type mConvergenceSpeedMultiplier;
	Type mStepPerSecond;
	Type mCurrentValue;
	Type mGoalValue;
	
}; // class InertValue


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ACC_INERT_VALUE_HPP__