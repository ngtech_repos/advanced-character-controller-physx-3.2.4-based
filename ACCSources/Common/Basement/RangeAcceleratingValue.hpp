/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#ifndef __ACC_RANGE_ACCELERATING_VALUE_HPP__
#define __ACC_RANGE_ACCELERATING_VALUE_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
class RangeAcceleratingValue
{

public:

	RangeAcceleratingValue( T velocity )
		:	mVelocity( velocity )
		,	mCurrent( 0 )
	{}

	void update( float timeStep, T goal )
	{
		if ( mCurrent > goal )
		{
			mCurrent -= mVelocity * timeStep;
			if ( mCurrent < goal )
				mCurrent = goal;
		}
		else
		{
			mCurrent += mVelocity * timeStep;
			if ( mCurrent > goal )
				mCurrent = goal;
		}
	}

	T getCurrent()
	{
		return mCurrent;
	}

	void setVelocity( T velocity )
	{
		mVelocity = velocity;
	}

private:

	T mCurrent;
	T mVelocity;
	
}; // class RangeAcceleratingValue


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


typedef RangeAcceleratingValue< float > RealRangeAcceleratingValue;


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ACC_RANGE_ACCELERATING_VALUE_HPP__