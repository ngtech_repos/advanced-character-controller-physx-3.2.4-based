/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#ifndef __CCBroadcaster_HPP__
#define __CCBroadcaster_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include "ACCSources/AutoConfiguration.hpp"
#include "ACCSources/CharacterController/AbstractCC.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class CCBroadcaster
	:	public AbstractCCInput
{

public:

	CCBroadcaster( std::set< AbstractCC * > & inputSet )
		:	mInputSet( inputSet )
	{}

	virtual void strafeLeftOn();
	virtual void strafeLeftOff();

	virtual void strafeRightOn();
	virtual void strafeRightOff();

	virtual void moveForwardOn();
	virtual void moveForwardOff();

	virtual void moveBackwardOn();
	virtual void moveBackwardOff();

	virtual void jumpOn();
	virtual void jumpOff();

	virtual void duckDown();
	virtual void duckUp();
	virtual void run();
	virtual void walk();

	virtual void resetInput();

private:

	std::set< AbstractCC * >& mInputSet;
};

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __CCBroadcaster_HPP__