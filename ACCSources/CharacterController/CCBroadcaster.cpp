/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#include "ACCSources/PCH/ACCPCH.hpp"
#include "ACCSources/CharacterController/CCBroadcaster.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#define ALL_CALL_BODY( _CALL_NAME )											\
void																		\
CCBroadcaster::_CALL_NAME()													\
{																			\
	std::set< AbstractCC * >::iterator begin = mInputSet.begin();			\
	std::set< AbstractCC * >::iterator end = mInputSet.end();				\
	while( begin != end )													\
	{																		\
		(*begin)->_CALL_NAME();												\
		++begin;															\
	}																		\
}																			\


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


ALL_CALL_BODY(strafeLeftOn);
ALL_CALL_BODY(strafeLeftOff);

ALL_CALL_BODY(strafeRightOn);
ALL_CALL_BODY(strafeRightOff);

ALL_CALL_BODY(moveForwardOn);
ALL_CALL_BODY(moveForwardOff);

ALL_CALL_BODY(moveBackwardOn);
ALL_CALL_BODY(moveBackwardOff);

ALL_CALL_BODY(jumpOn);
ALL_CALL_BODY(jumpOff);

ALL_CALL_BODY(duckDown);
ALL_CALL_BODY(duckUp);

ALL_CALL_BODY(run);
ALL_CALL_BODY(walk);

ALL_CALL_BODY(resetInput);


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
