/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#ifndef __ACC_CharacterControllerGameParameters_HPP__
#define __ACC_CharacterControllerGameParameters_HPP__

#include "ACCSources/CharacterController/AbstractCC.hpp"
#include "ACCSources/CharacterController/CCInput.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class  CCGameParameters
	:	public CCInput
{

public:

	CCGameParameters();

	virtual float getControllerMinHeight() const														{ return mControllerMinHeight;		}
	virtual float getControllerMaxHeight() const														{ return mControllerMaxHeight;		}
	virtual float getPlayerMass() const																	{ return mPlayerMass;				}

	virtual float getDuckingDownVelocity() const														{ return mDuckingDownVelocity;		}
	virtual float getDuckingUpVelocity() const															{ return mDuckingUpVelocity;		}
	virtual float getStrafeSidesCoefficient() const														{ return mStrafeSidesCoefficient;	}

	virtual void setControllerMinHeight( float value );
	virtual void setControllerMaxHeight( float value );

	virtual void setPlayerMass( float value );
	virtual void setDuckingDownMultiplier( float value );
	virtual void setDuckingUpMultiplier( float value );
	virtual void setStrafeSidesCoefficient( float value );

	virtual const CCMovementProperties& getWalkMovementProperties() const									{ return mWalkMovementProperties;}
	virtual void setWalkMovementProperties( const CCMovementProperties& valueproperties );

	virtual const CCMovementProperties& getRunMovementProperties() const									{ return mRunMovementProperties; }
	virtual void setRunMovementProperties( const CCMovementProperties& valueproperties );

	virtual const CCAfterJumpDuckProperties& getAfterJumpDuckProperties() const								{ return mAfterJumpDuckScenario; }
	virtual void setAfterJumpDuckProperties( const CCAfterJumpDuckProperties& valueproperties );

	virtual const CCSlopeMovementLimitationsProperties& getSlopeMovementLimitationsProperties() const		{ return mSlopeProperties; }
	virtual void setSlopeMovementLimitationsProperties( CCSlopeMovementLimitationsProperties& valueproperties );

	virtual const CCJumpProperties& getJumpProperties() const												{ return mJumpProperties; }
	virtual void setJumpProperties( CCJumpProperties& value );

private:

	void makeDefaultAfterJumpDuckProperties();
	void makeDefaultMovementProperties();
	void makeDefaultJumpProperties();
	void makeDefaultSlopeMovementProps();

	void setMovementProperties( const CCMovementProperties& valueproperties, CCMovementProperties& valuedestination );

protected:


	struct AfterJumpDuckScenario
		:	public CCAfterJumpDuckProperties
	{
		bool mStayDuckedPhysicLogic;
		float mCurrentDuckTimeCounter;

	}; // struct AfterJumpDuckProperties


	AfterJumpDuckScenario mAfterJumpDuckScenario;	
	CCMovementProperties mRunMovementProperties;
	CCMovementProperties mWalkMovementProperties;
	CCJumpProperties mJumpProperties;
	CCSlopeMovementLimitationsProperties mSlopeProperties;

private:

	float mControllerMinHeight;
	float mControllerMaxHeight;
	float mPlayerMass;
	float mDuckingDownVelocity;
	float mDuckingUpVelocity;
	float mStrafeSidesCoefficient;

}; // class CharacterControllerGameParameters


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ACC_CharacterControllerGameParameters_HPP__
