/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#include "ACCSources/PCH/ACCPCH.hpp"
#include "ACCSources/CharacterController/CCProperties.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


CCDescription::CCDescription ()
	:	mFrictionAdapter( 0 )
	,	mMass( 1 )
	,	mPxMoveStepOffset( 0.5f )
	,	mOrientationAngleXZ( degrees( 0 ) )
	,	mFixedTimeStepSize( -1 )
{}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


CCShapeDescription::CCShapeDescription()
	:	mPosition( 0 )
	,	mCurrentHeight( 2 )
	,	mRadius( 0.20f )
{
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


bool
CCShapeDescription::isValid() const
{
	return mCurrentHeight - 2 * mRadius >= 0.0f	&&	mRadius > 0;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCMovementProperties::calculateCurrentVelocityProperties(
		float& currentAcceleration
	,	float& currentMaxVelocity
	,	float minHeight
	,	float maxHeight
	,	float currentHeight
	,	float currentSlopeAngleDegree
)
{
	float currentFlatMaxVelocity = getCurrentResultByProportion(
			minHeight
		,	maxHeight
		,	currentHeight
		,	mMinHeightFlatMaxVelocity
		,	mMaxHeightFlatMaxVelocity
	);

	float currentMinHeightFlatAcceleration = getCurrentResultByProportion(
			minHeight
		,	maxHeight
		,	currentHeight
		,	mMinHeightAcceleration
		,	mMaxHeightAcceleration
	);

	float ascentDescentMultiplier;
	if ( currentSlopeAngleDegree < 0 )
	{
		ascentDescentMultiplier = mDescentInfluence.getCurrentMovementInfluence( currentSlopeAngleDegree );
		clamp( mDescentInfluence.mMaxSlowdownCoefficient, 1.0f, ascentDescentMultiplier );
		
		//ibah42 - don't remember where is this need to!
		//ascentDescentMultiplier *= degrees( currentSlopeAngleDegree ).cos() ) );
	}
	else
	{
		ascentDescentMultiplier = mAscentInfluence.getCurrentMovementInfluence( currentSlopeAngleDegree );
		clamp( mAscentInfluence.mMaxSlowdownCoefficient, 1.0f, ascentDescentMultiplier );
	}
	
	Assert ( ascentDescentMultiplier <= 1.0f && ascentDescentMultiplier >= 0 );

	currentMaxVelocity = currentFlatMaxVelocity * ascentDescentMultiplier;
	currentAcceleration = currentMinHeightFlatAcceleration * ascentDescentMultiplier;

} // MovementProperties::calculateCurrentVelocityProperties


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


float
CCMovementProperties::getCurrentResultByProportion(
		float minSample
	,	float maxSample
	,	float currentSample
	,	float resultMin
	,	float resultMax
)
{
	if ( minSample == currentSample )
		return resultMin;
	if ( maxSample == currentSample )
		return resultMax;

	float divider1 = returnCorrectDivider( maxSample - currentSample );
	float k = returnCorrectDivider( currentSample - minSample ) / divider1;
	float divider2 = returnCorrectDivider( k + 1.f );
	return ( resultMax * k + resultMin ) / divider2;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


float
CCMovementProperties::SlopeProperties::getCurrentMovementInfluence( float angleDegree ) const
{
	if( angleDegree < 0 )
		angleDegree = - angleDegree;

	assertNonNullDivider( mMaxAngleDegrees );

	float k = ( mMaxSlowdownCoefficient - 1.f ) / mMaxAngleDegrees;
	float b = 1.0f;

	return k * angleDegree + b;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


float
CCJumpProperties::getOriginJumpVelocity( float gravity, float pastAccelPath ) const
{
	float k;
	if ( mIdealAccelPath > 0 )
		k = pastAccelPath / mIdealAccelPath;
	else
		k = 1;

	Assert( k >= 0 && k <= 1 );

	clamp( 0.f, mIdealAccelPath, pastAccelPath );
	return sqrt(
			2
		*	gravity
		*	mJumpMaxHeight
		*	k
	);

} // CCJumpProperties::getOriginJumpVelocity


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


float
DefaultCCFrictionAdapter::getTableAdaptation( float friction ) const
{
	Assert( friction >= 0 && friction < 1.001f );

	if ( friction < 0.1f )
		return friction * 4.0f;
	else if ( friction < 0.3f )
		return friction * 3.1f; // be carefull!
	else
		return 1.0f;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


float
DefaultCCFrictionAdapter::getDynamicFriction( const PxMaterial* mat ) const
{
	if ( mat )
		return getTableAdaptation( mat->getDynamicFriction() );
	else
		return getTableAdaptation( 0 );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


float
DefaultCCFrictionAdapter::getStaticFriction( const PxMaterial* mat ) const
{
// 	PxMaterial* material = 0;
// 	mscene-> getPhysics().getMaterials( &material, 1, id );
	if ( mat )
		return getTableAdaptation( mat->getStaticFriction() );
	else
		return getTableAdaptation( 0 );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/