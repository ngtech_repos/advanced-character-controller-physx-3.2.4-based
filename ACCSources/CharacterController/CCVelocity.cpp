/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#include "ACCSources/PCH/ACCPCH.hpp"
#include "ACCSources/CharacterController/CCVelocity.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


CCVelocity::CCVelocity()
	:	mLocalVel( 0 )
	,	mGlobalNewMandatoryVel( 0 )
	,	mGlobalOptionalVel( 0 )
	,	mGlobalUsedMandatoryVel( 0 )
	,	mG( 9.8f )
	,	mStaticFriction( 0 )
	,	mDynamicFriction( 0 )
	,	mStopVelocityMultiplier( 0 )
	,	mDt( 0 )
	,	mMinStopVel( 0 )
	,	mMaxStopVel( 0 )
	,	mMaxSlitheringVel(0)
	,	mSlitheringVel(0)
{
	mBufferedGlobalOptionalAcceleration.reserve(4);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCVelocity::update(
		float timeStep
	,	InVec3 localAcceleration
	,	float maxVelocity
	,	float minStopVelocity
	,	float maxStopVelocity
	,	float stopVelocityMultiplier
	,	float dynamicFriction
	,	float staticFriction
	,	float G
	,	InVec3 prevGlobalVelocity
)
{
	mG = G;
	mDt = timeStep;
	mStaticFriction = staticFriction;
	mDynamicFriction = dynamicFriction;
	mMaxStopVel = maxStopVelocity;                                                                                                   
	mMinStopVel = minStopVelocity;
	mStopVelocityMultiplier = stopVelocityMultiplier;
	mMaxLocalVel = maxVelocity;

	// if this logic should be changed - also check LandingPattern, stayGroundPattern which depends on mLocalVel.y!
	if ( localAcceleration.y <= 0 && mLocalVel.y > -200.f )
		mLocalVel.y -= mG * mDt;

	if ( ! isZero( localAcceleration ) )
	{
		float angle = getXZAngleBetweenVectors( localAcceleration, mLocalVel );
		if ( angle >= 90.0f || angle <= -90.0f )
			consumeVelocityXZ( mLocalVel, getRadialMethodStopVel( mLocalVel ) );

		applyAccelerationAndDecelerationAsComposed( mLocalVel, localAcceleration, false, true );		
	}
	else
		consumeVelocityXZ( mLocalVel, getRadialMethodStopVel( mLocalVel ) );


	if( mSlitheringAccel )
	{
		accelerateXYZRadialMethod( *mSlitheringAccel, mMaxSlitheringVel, mSlitheringVel, true );
		mSlitheringAccel.reset();
	}
	else
		consumeVelocityXZ( mSlitheringVel, getRadialMethodStopVel( mSlitheringVel ) );

	if ( localAcceleration.y > 0 )
		mLocalVel.y += prevGlobalVelocity.y;

	updateGlobalMandatoryVel();
	updateGlobalOptionalVel();

	mVelocity = mGlobalOptionalVel + mLocalVel + mSlitheringVel;

	applyMandatoryVelocity( mGlobalUsedMandatoryVel.x, mVelocity.x );
	applyMandatoryVertiaclYVelocity( mGlobalUsedMandatoryVel.y, mVelocity.y );
	applyMandatoryVelocity( mGlobalUsedMandatoryVel.z, mVelocity.z );

} // CCVelocity::update



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCVelocity::consumeVelocityXZ( PxVec3& velocity, float stopVel )
{
	if ( mStaticFriction <= 0 || stopVel <= 0 )
		return;

	float consumeVelocityMagnitude = stopVel * mDt * mStaticFriction;

	float originVelocityMagnitudeSquared = returnCorrectDivider( magnitudeXZSquared( velocity ) );
	if ( consumeVelocityMagnitude * consumeVelocityMagnitude >= originVelocityMagnitudeSquared )
	{
		velocity.x = 0;
		velocity.z = 0;
	}
	else
	{	
		originVelocityMagnitudeSquared = sqrt( originVelocityMagnitudeSquared );

		float newVelocityMagnitude = originVelocityMagnitudeSquared - consumeVelocityMagnitude;
		
		Assert ( newVelocityMagnitude <= originVelocityMagnitudeSquared );
		
		float ratio = newVelocityMagnitude / originVelocityMagnitudeSquared;
		velocity.x *= ratio;
		velocity.z *= ratio;
	}

} // CCVelocity::consumeVelocityXZ


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCVelocity::consumeUnexpressedVelocity( InVec3 theoreticalMove, InVec3 realMove )
{
	float xr = realMove.x / returnCorrectDivider( theoreticalMove.x );
	clamp( 0, 1.0f, xr );

	float yr = realMove.y / returnCorrectDivider( theoreticalMove.y );
	clamp( 0, 1.0f, yr );

	float zr = realMove.z / returnCorrectDivider( theoreticalMove.z );
	clamp( 0, 1.0f, zr );

	mGlobalUsedMandatoryVel.x *= xr;
	mGlobalUsedMandatoryVel.y *= yr;
	mGlobalUsedMandatoryVel.z *= zr;

	mGlobalOptionalVel.x *= xr;
	mGlobalOptionalVel.y *= yr;
	mGlobalOptionalVel.z *= zr;

	mLocalVel.x *= xr;
	mLocalVel.y *= yr;
	mLocalVel.z *= zr;

	mSlitheringVel.x *= xr;
	mSlitheringVel.y *= yr;
	mSlitheringVel.z *= zr;
	
} // CCVelocity::consumeUnexpressedVelocity


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCVelocity::applyAccelerationAndDecelerationAsComposed(
		PxVec3& usedVel
	,	InVec3 appliedAcceleration
	,	bool optionalGlobalInitiatedStopVelocityUsed
	,	bool localInitiatedStopVelocityUsed
)
{
	if ( isZeroXZ( appliedAcceleration ) )
	{
		consumeVelocityXZ( usedVel, getRadialMethodStopVel( usedVel ) );
	
		if ( localInitiatedStopVelocityUsed )
			usedVel.y += appliedAcceleration.y * mDt;
	}
	else
	{
		float initiatedMagnitude = 0;
		float errataMagnitude = 0;

		PxVec3 initiated = appliedAcceleration;
		initiated.y = 0;
		initiated.normalize();
		
		PxVec3 errata = initiated.cross( PxVec3( 0,1,0 ) );
		errata.normalize();
		Assert( errata.y == 0 );

		PxVec3 testPlus = usedVel + errata;
		testPlus.y = 0;

		PxVec3 testMinus = usedVel - errata;
		testMinus.y = 0;

		if ( testMinus.magnitudeSquared() > testPlus.magnitudeSquared() )
			errata = -errata;
		
		decomposeVec3XZ( usedVel, initiated, errata, initiatedMagnitude, errataMagnitude );

#ifdef ACC_DEBUG

		PxVec3 vel = usedVel;
		vel.y = 0;

		PxVec3 calcVel = errata * errataMagnitude + initiated * initiatedMagnitude;
		calcVel.y = 0;
		Assert( equalVec3( vel, calcVel ) );
		Assert(
				errata.isNormalized() && initiated.isNormalized()
			||	errata.magnitudeSquared() < 0.000001f
			||	initiated.magnitudeSquared() < 0.000001f
		);

#endif // ACC_DEBUG

		if ( errataMagnitude > 0.000001f )
		{
			applyDecomposedErrataStopVelocity( usedVel, errata, errataMagnitude );
		}

		if ( optionalGlobalInitiatedStopVelocityUsed )
		{
			float maxVel = mBufferedGlobalOptionalAcceleration.at( mBufferedGlobalOptionalAcceleration.size()-1 ).maxVel;
			
			applyDecomposedInitiatedAcceleration( usedVel, initiated, initiatedMagnitude, maxVel, appliedAcceleration );
			applyDecomposedInitiatedStopVelocity( usedVel, initiated, initiatedMagnitude, maxVel );
		}

		if ( localInitiatedStopVelocityUsed )
		{
			applyDecomposedInitiatedAcceleration( usedVel, initiated, initiatedMagnitude, mMaxLocalVel, appliedAcceleration );
			applyDecomposedInitiatedStopVelocity( usedVel, initiated, initiatedMagnitude, mMaxLocalVel );
		}
	}

} // CCVelocity::decomposeVelocityAndApplyStopVelocity


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void 
CCVelocity::applyDecomposedInitiatedAcceleration( PxVec3& usedVel, InVec3 initiatedNormal, float initiatedMagnitude, float maxVel, InVec3 acceleration )
{

#ifdef ACC_DEBUG
	float a = getXZAngleBetweenVectors( initiatedNormal, acceleration );
	Assert( fabs(a) < 0.1f );
#endif // ACC_DEBUG


	PxVec3 appliedVel = acceleration * mDt;
	appliedVel.x *= mDynamicFriction;
	appliedVel.z *= mDynamicFriction;

	//Y is just applied!
	usedVel.y += appliedVel.y;

	if ( initiatedMagnitude >= maxVel )
		return;

	float appliedVelMag = magnitudeXZ( appliedVel );
	if ( appliedVelMag + initiatedMagnitude > maxVel )
	{
		float correctionKoefficient = maxVel - initiatedMagnitude;
		correctionKoefficient = correctionKoefficient / appliedVelMag;
		
		usedVel.x += appliedVel.x * correctionKoefficient;
		usedVel.z += appliedVel.z * correctionKoefficient;
	}
	else
	{
		usedVel.x += appliedVel.x;
		usedVel.z += appliedVel.z;
	}

} // CCVelocity::applyDecomposedAcceleration


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCVelocity::applyDecomposedInitiatedStopVelocity( PxVec3& usedVel, InVec3 initiatedNormal, float initiatedMagnitude, float maxVel )
{
	if ( initiatedMagnitude <= maxVel )
		return;

	Assert( initiatedNormal.isNormalized() );

	float stopVelMag = initiatedMagnitude * mStopVelocityMultiplier;
	clamp( mMinStopVel, mMaxStopVel, stopVelMag );

	stopVelMag *= mDt * mStaticFriction;

	if ( stopVelMag > initiatedMagnitude - maxVel )
		stopVelMag = initiatedMagnitude - maxVel;

	usedVel -= initiatedNormal * stopVelMag;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCVelocity::applyDecomposedErrataStopVelocity( PxVec3& usedVel, InVec3 errataNormal, float errataMagnitude )
{	
	Assert( errataNormal.isNormalized() );

	float stopVelMag = errataMagnitude * mStopVelocityMultiplier;
	clamp( mMinStopVel, mMaxStopVel, stopVelMag );

	stopVelMag *= mDt * mStaticFriction;

	if ( stopVelMag > errataMagnitude )
		stopVelMag = errataMagnitude;

	usedVel -= errataNormal * stopVelMag;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCVelocity::setSlitheringAcceleration( PxVec3 accel, float maxVel )
{
	Assert( !mSlitheringAccel && maxVel > 0 );

	mSlitheringAccel = accel;
	mMaxSlitheringVel = maxVel;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCVelocity::applyMandatoryVertiaclYVelocity( float mandatory, float &result )
{
	if ( mandatory == 0 )
		return;

	if ( mandatory > 0 )
	{
		if ( result > 0 )
		{
			result += mandatory;
			return;
		}
		else
			result = mandatory;
	}
	else
	{
		if ( result < mandatory )
		{
			result += mandatory;
			return;
		}
		else
			result = mandatory;
	}
}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void 
CCVelocity::updateGlobalMandatoryVel()
{
	float stopVel = mGlobalUsedMandatoryVel.magnitude() * mStopVelocityMultiplier;
	clamp( mMinStopVel, mMaxStopVel, stopVel );
	
	consumeVelocityXZ( mGlobalUsedMandatoryVel, stopVel );
	correctYVelocity( mGlobalUsedMandatoryVel );
	
	chooseSupremeOrthValue( mGlobalNewMandatoryVel, mGlobalUsedMandatoryVel );

	mGlobalNewMandatoryVel = PxVec3(0);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCVelocity::updateGlobalOptionalVel()
{
	std::vector< BufferedAcceleration >::iterator begin = mBufferedGlobalOptionalAcceleration.begin();
	std::vector< BufferedAcceleration >::iterator end =  mBufferedGlobalOptionalAcceleration.end();
	std::sort( begin, end );

	PxVec3 appliedGlobalOptionalVel(0);
	while( begin != end )
	{
		Assert( (*begin).accel.y == 0 );
		accelerateXYZRadialMethod( (*begin).accel, (*begin).maxVel, appliedGlobalOptionalVel, true );
		++begin;
	}

	applyAccelerationAndDecelerationAsComposed( mGlobalOptionalVel, appliedGlobalOptionalVel / mDt, true, false );
	mBufferedGlobalOptionalAcceleration.clear();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCVelocity::addGlobalMandatoryVelocity( InVec3 velocity )
{
	chooseSupremeOrthValue( velocity, mGlobalNewMandatoryVel );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCVelocity::chooseSupremeOrthValue( InVec3 add, PxVec3& result )
{
	chooseSupremeOrthValue( add.x, result.x );
	chooseSupremeOrthValue( add.y, result.y );
	chooseSupremeOrthValue( add.z, result.z );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCVelocity::applyMandatoryVelocity( float mandatory, float& result )
{
	if ( mandatory == 0 )
		return;

	if ( mandatory > 0 )
	{
		if ( result > mandatory )
			return;
		else
			result = mandatory;
	}
	else
	{
		if ( result < mandatory )
			return;
		else
			result = mandatory;
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCVelocity::chooseSupremeOrthValue( float add, float & result )
{
	if ( result <= 0 )
	{
		if ( add <= 0 )
			result = minimal( result, add );
		else
		{
			// this could mean that we are going to stuck
			result += add;
		}
	}
	else
	{
		if ( add <= 0 )
		{
			// this could mean that we are going to stuck
			result += add;
		}
		else
			result = maximal( result, add  );		
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCVelocity::addGlobalOptionalAcceleration( PxVec3 accel, float maxVel )
{
	Assert( accel.y == 0 && maxVel > 0 );
	mBufferedGlobalOptionalAcceleration.push_back( BufferedAcceleration( accel, maxVel ) );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCVelocity::accelerateXYZRadialMethod( InVec3 accel, float maxVel, PxVec3& result, bool checkMaxVel )
{
	result.x += mDynamicFriction * accel.x * mDt;
	result.y += accel.y * mDt;
	result.z += mDynamicFriction * accel.z * mDt;
		
	if ( !checkMaxVel )
		return;

	float velocityMagnitudeSquared = returnCorrectDivider(
		magnitudeXZSquared( result )
	);

	if ( velocityMagnitudeSquared > maxVel*maxVel )
	{
		float ratio = maxVel / sqrt( velocityMagnitudeSquared );
		result.x *= ratio;
		result.z *= ratio;
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


float
CCVelocity::getRadialMethodStopVel( InVec3 sourceVel )
{
	float stopVel = magnitudeXZ( sourceVel );
	stopVel *= mStopVelocityMultiplier;
	clamp( mMinStopVel, mMaxStopVel, stopVel );
	return stopVel;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

