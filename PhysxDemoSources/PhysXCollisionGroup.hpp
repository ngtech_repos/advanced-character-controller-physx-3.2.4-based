/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#ifndef __PhysX_COLLISION_GROUP_HPP__
#define __PhysX_COLLISION_GROUP_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace ACC;
using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#define COLLISION_GROUPS_COUNT 32

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

class PhysxCollisionGroup
{

public:

	PhysxCollisionGroup();

	void setU32CollisionFlag(
			PxU32 firstGroupNumber
		,	PxU32 secondGroupNumber
		,	bool enable
	);

	PxU32 * getCollisionGroups()		{ return mGroupCollisionFlags; }
	int getCollisionGroupsSizeBytes()	{ return COLLISION_GROUPS_COUNT * sizeof( PxU32 ); }

private:

	PxU32 mGroupCollisionFlags[ COLLISION_GROUPS_COUNT ];


}; // class CollisionGroup


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __PhysX_COLLISION_GROUP_HPP__
