/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#include "PhysxDemoSources/PCH.hpp"
#include "PhysxDemoSources/PhysXCollisionGroup.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


PhysxCollisionGroup::PhysxCollisionGroup()
{
	//init all group pairs as true:
	for ( unsigned i = 0; i < COLLISION_GROUPS_COUNT; i ++)
		mGroupCollisionFlags[ i ] = 0xFFFFffff;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
PhysxCollisionGroup::setU32CollisionFlag(
		PxU32 firstGroupNumber
	,	PxU32 secondGroupNumber
	,	bool enable
)
{
	Assert(
			firstGroupNumber < COLLISION_GROUPS_COUNT
		&&	secondGroupNumber < COLLISION_GROUPS_COUNT
	);
	if ( enable )
	{
		//be symmetric:
		mGroupCollisionFlags[ firstGroupNumber  ] |= ( 1 << secondGroupNumber );
		mGroupCollisionFlags[ secondGroupNumber ] |= ( 1 << firstGroupNumber );
	}
	else
	{
		mGroupCollisionFlags[ firstGroupNumber  ] &= ~( 1 << secondGroupNumber );
		mGroupCollisionFlags[ secondGroupNumber ] &= ~( 1 << firstGroupNumber );
	}

} // CollisionGroup::setU32CollisionFlag


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
