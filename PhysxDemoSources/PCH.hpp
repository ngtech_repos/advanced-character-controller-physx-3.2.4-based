/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#ifndef __PCH_HPP__
#define __PCH_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include <ctime>
#include <iostream>
#include <limits.h>
#include <float.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <hash_set>

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include <PxPhysicsAPI.h>
#include <foundation/PxVec3.h>
typedef const physx::PxVec3& InVec3;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include <windows.h>
#include <GL/gl.h>
#include <CommonSources/Glut/GL/glut.h>

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include "ACCSources/AutoConfiguration.hpp"

#include "ACCSources/Common/Basement/Optional.hpp"
#include "ACCSources/Common/Basement/RangeAcceleratingValue.hpp"
#include "ACCSources/Common/Basement/InertValue.hpp"
#include "ACCSources/Common/Basement/Switch.hpp"
#include "ACCSources/Common/Basement/Utility.hpp"
#include "ACCSources/Common/Basement/CosineInterpolationCurve.hpp"
#include "ACCSources/Common/Basement/Angle.hpp"
#include "ACCSources/Common/Basement/Rotation.hpp"

#include "ACCSources/CharacterController/AbstractCC.hpp"
#include "ACCSources/CharacterController/CCManager.hpp"


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#define CRITICAL_MESSAGE( _MSG )					\
{													\
	MessageBox(										\
			NULL									\
		,	(LPCWSTR)(_MSG)							\
		,	(LPCWSTR)"Critical error occured"		\
		,	MB_OK | MB_ICONERROR | MB_TASKMODAL		\
	);												\
													\
	std::string str( "Critical error occured " );	\
	str += (const char*)(__FILE__);					\
	str += "  ";									\
	str += (const char*)(__LINE__);					\
	throw std::exception( str.c_str() );			\
}													\


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __PCH_HPP__
