/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/


#include "PhysxDemoSources/PCH.hpp"
#include "PhysxDemoSources/Camera.hpp"


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


PxMat33
get3x3RotMatrix(const float degrees, const PxVec3& up )
{
	assert( up.isNormalized() );
	const float rad = degrees * ACC::Pi / 180.f;
	const float c = cos( rad );
	const float s = sin( rad );
	const float mc = 1.f - c;

	return PxMat33(
			PxVec3(
					up.x * up.x * mc	+	c	
				,	up.x * up.y * mc	-	up.z * s
				,	up.x * up.z * mc	+	up.y * s
			)
		,	PxVec3(	
					up.y * up.x * mc	+	up.z * s
				,	up.y * up.y * mc	+	c
				,   up.z * up.y * mc	-	up.x * s
			)
		,	PxVec3(
					up.z * up.x * mc	-	up.y * s
				,	up.z * up.y * mc	+	up.x * s
				,	up.z * up.z * mc	+	c
			)
	);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


const PxVec3&
Camera::rotate( float _xDeg, float _yDeg )
{
	_yDeg = -_yDeg;
	
	ACC::clamp( mLowDegCone, mHighDegCone, _yDeg );
	ACC::clamp( -180.f, 180.f, _xDeg );
	
	float targetDegree = currentPitchDegree() + _yDeg;
	mDir.y = 0;
	ACC::clamp( mLowDegCone, mHighDegCone, targetDegree );
	ACC::rotateXZ( mDir, _xDeg );
	mDir.normalize();
	
	ACC::Angle a = ACC::Angle::buildDegrees( targetDegree );
	mDir.y = a.sin();
	mDir.x *= a.cos();
	mDir.z *= a.cos();
	mDir.normalize();

	float pitchDegree = currentPitchDegree();
	assert ( ACC::cmpEqual( pitchDegree, targetDegree, 0.01f ) );	
	assert ( ACC::cmpSmaller( pitchDegree, mHighDegCone, 0.0001f ) );
	assert ( ACC::cmpBigger( pitchDegree, mLowDegCone, 0.0001f ) );
	
	return mDir;

} // Camera::rotate


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


Camera::Camera( PxVec3& pos, PxVec3& dir, PxVec3 up, float lowDegCone, float highDegCone )
	:	mPos( pos )
	,	mDir( dir )
	,	mUp( up )
	,	mLowDegCone( lowDegCone )
	,	mHighDegCone( highDegCone )
{
	mDir.normalize();
	mUp.normalize();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Camera::defaultPrefsLookAt()
{
	gluLookAt ( 
			pos().x
		,	pos().y
		,	pos().z
	
		,	-dir().x * 100.f + pos().x
		,	dir().y * 100.f + pos().y
		,	dir().z * 100.f + pos().z
		
		,	up().x
		,	up().y
		,	up().z
	);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


float
Camera::currentPitchDegree() const
{
	return ACC::Angle::atan(
		mDir.y / ::sqrt( mDir.x * mDir.x  +  mDir.z * mDir.z )
	).getDegrees();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/