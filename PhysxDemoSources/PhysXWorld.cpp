/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#include "PhysxDemoSources/PCH.hpp"
#include "PhysxDemoSources/PhysXWorld.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


PxFilterFlags PhysxWorld::getSimulationFilterCallback(
		PxFilterObjectAttributes attributes0
	,	PxFilterData filterData0
	,	PxFilterObjectAttributes attributes1
	,	PxFilterData filterData1
	,	PxPairFlags& pairFlags
	,	const void* constantBlock
	,	PxU32 constantBlockSize
)
{
	bool result =
			PxFilterObjectType::eRIGID_DYNAMIC == PxGetFilterObjectType( attributes0 )
		||	PxFilterObjectType::eRIGID_DYNAMIC == PxGetFilterObjectType( attributes1 )
	;
	if ( result )
		result = !PxFilterObjectIsKinematic( attributes0 ) || !PxFilterObjectIsKinematic( attributes1 );

	if( result )
	{
		pairFlags |=
				PxPairFlag::eSWEPT_INTEGRATION_LINEAR
			|	PxPairFlag::eRESOLVE_CONTACTS
			|	PxPairFlag::eNOTIFY_TOUCH_FOUND
			|	PxPairFlag::eNOTIFY_CONTACT_POINTS
		;
	}
	else
	{
		pairFlags |=
				PxPairFlag::eNOTIFY_TOUCH_FOUND
			|	PxPairFlag::eNOTIFY_CONTACT_POINTS
		;
	}

	PxU32 ShapeGroup0 = filterData0.word0 & 31;
	PxU32 ShapeGroup1 = filterData1.word0 & 31;
	PxU32* groupCollisionFlags = (PxU32*)constantBlock;

	if ( (groupCollisionFlags[ShapeGroup0] & (1 << ShapeGroup1)) == 0)
		return PxFilterFlag::eSUPPRESS;
	else
		return PxFilterFlag::eDEFAULT;

	return PxFilterFlags();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


PhysxWorld::PhysxWorld()
	:	mFoundation(0)
	,	mProfileZoneManager(0)
	,	mCudaContextManager(0)
	,	mCooking(0)
	,	mPhysics(0)
	,	mControllerManager(0)
	,	mCpuDispatcher(0)
	,	mDefaultMaterial(0)
	,	mGpuDispatcher(0)
	,	mIsCorrect(false)
{
	mDefaultToleranceScale.length = 1;
	mDefaultToleranceScale.mass = 1000;
	mDefaultToleranceScale.speed = ACC::G;
	if( !mDefaultToleranceScale.isValid() )
	{
		release();
		CRITICAL_MESSAGE( "mDefaultToleranceScale is not valid" );
	}
	
	/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	//1
	mFoundation = PxCreateFoundation(
			PX_PHYSICS_VERSION
		,	mDefaultAllocatorCallback
		,	mDefaultErrorCallback
	);
	if ( ! mFoundation )
	{
		release();
		CRITICAL_MESSAGE( "PxCreateFoundation failed" );
	}

	/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	//2
	mProfileZoneManager = & PxProfileZoneManager::createProfileZoneManager( mFoundation	);
	if ( ! mProfileZoneManager )
	{
		release();
		CRITICAL_MESSAGE( "PxProfileZoneManager::createProfileZoneManager failed" );
	}

	/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	//3
#ifdef PX_WINDOWS
	pxtask::CudaContextManagerDesc cudaContextManagerDesc;
	mCudaContextManager = pxtask::createCudaContextManager( *mFoundation, cudaContextManagerDesc, mProfileZoneManager );
	if( mCudaContextManager )
	{
		if( !mCudaContextManager->contextIsValid() )
		{
			mCudaContextManager->release();
			mCudaContextManager = 0;
		}
	}
#endif
	
	/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	//4
	mPhysics = PxCreatePhysics(
			PX_PHYSICS_VERSION
		,	* mFoundation
		,	PxTolerancesScale()
		,	true
		,	mProfileZoneManager
	);
	if ( ! mPhysics )
	{
		release();
		CRITICAL_MESSAGE( "PxCreatePhysics failed" );
	}

	/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	//5
	if( ! PxInitExtensions( *mPhysics ) )
	{
		release();
		CRITICAL_MESSAGE( "PxInitExtensions failed" );
	}

	/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
	
	//6
	mCooking = PxCreateCooking( PX_PHYSICS_VERSION, *mFoundation, PxCookingParams( mDefaultToleranceScale ) );
	if( ! mCooking )
	{
		release();
		CRITICAL_MESSAGE( "PxCreateCooking failed" );
	}

	togglePvdConnection();

	if( getPhysics().getPvdConnectionManager() )
		getPhysics().getPvdConnectionManager()->addHandler( *this );



	/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	//7
	mControllerManager = PxCreateControllerManager( *mFoundation );
	if( ! mControllerManager )
	{
		release();
		CRITICAL_MESSAGE("PxCreateControllerManager failed" );
	}

	/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	//8
	int nbThreads = 2;

#ifdef PX_WINDOWS
	SYSTEM_INFO sysinfo;
	GetSystemInfo( &sysinfo );
	nbThreads = sysinfo.dwNumberOfProcessors;
#endif

	mCpuDispatcher = PxDefaultCpuDispatcherCreate( nbThreads );
	if( ! mCpuDispatcher )
	{
		release();
		CRITICAL_MESSAGE( "PxDefaultCpuDispatcherCreate failed" );
	}

	/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
	
	//9
	mDefaultMaterial = mPhysics->createMaterial( 0.3f, 0.3f, 0.1f );
	if( ! mDefaultMaterial )
	{
		release();
		CRITICAL_MESSAGE( "mPhysics->createMaterial failed" );
	}

	/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	//10
#ifdef PX_WINDOWS
	if( mCudaContextManager )
		mGpuDispatcher = mCudaContextManager->getGpuDispatcher();
#endif

	/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	//11
	mControllerManager = PxCreateControllerManager( *mFoundation );
	if( ! mControllerManager )
	{
		release();
		CRITICAL_MESSAGE( "PxCreateControllerManager failed" );
	}

	mControllerManager->setDebugRenderingFlags( PxU32(PxControllerDebugRenderFlags::eALL) );

	mIsCorrect = true;

} // World::World


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


PhysxWorld::~PhysxWorld()
{
	release();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
PhysxWorld::release()
{
	if ( mGpuDispatcher )
		mGpuDispatcher->stopSimulation();

	if ( mCpuDispatcher )
		mCpuDispatcher->release();

	if( mControllerManager )
	{
		mControllerManager->purgeControllers();
		mControllerManager->release();
	}

	if( mCooking )
		mCooking->release();

	PxCloseExtensions();

	if(  mPhysics )
		mPhysics->release();

#ifdef PX_WINDOWS
	if(  mCudaContextManager )
		mCudaContextManager->release();
#endif

	if( mProfileZoneManager )
		mProfileZoneManager->release();

	if( mFoundation )
		mFoundation->release();

} // World::release


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
PhysxWorld::togglePvdConnection()
{
	if( !getPhysics().getPvdConnectionManager() )
		return;
	if ( getPhysics().getPvdConnectionManager()->isConnected() )
		getPhysics().getPvdConnectionManager()->disconnect();
	else
		createPvdConnection();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
PhysxWorld::createPvdConnection()
{
	Assert( getPhysics().getPvdConnectionManager() );
	
	//The connection flags state overall what data is to be sent to PVD.  Currently
	//the Debug connection flag requires support from the implementation (don't send
	//the data when debug isn't set) but the other two flags, profile and memory
	//are taken care of by the PVD SDK.

	//Use these flags for a clean profile trace with minimal overhead
	PxVisualDebuggerConnectionFlags theConnectionFlags( PxVisualDebuggerExt::getAllConnectionFlags() );
	//if ( !mUseFullPvdConnection)
	//theConnectionFlags = PxVisualDebuggerConnectionFlag::Profile;

	//Create a pvd connection that writes data straight to the filesystem.  This is
	//the fastest connection on windows for various reasons.  First, the transport is quite fast as
	//pvd writes data in blocks and filesystems work well with that abstraction.
	//Second, you don't have the PVD application parsing data and using CPU and memory bandwidth
	//while your application is running.
	//PxVisualDebuggerExt::createConnection(getPhysics().getPvdConnectionManager(), "c:\\temp.pxd2", theConnectionFlags);
	
	const char* pvdHost = "127.0.0.1";
	PxU32 pvdPort = 5425U;
// 	if (mApplication.getCommandLine().hasSwitch("pvdhost"))
// 	{
// 		const char* hostStr = mApplication.getCommandLine().getValue("pvdhost");
// 		if (hostStr)
// 			pvdHost = hostStr;
// 	}
// 
// 	if (mApplication.getCommandLine().hasSwitch("pvdport"))
// 	{	
// 		const char* portStr = mApplication.getCommandLine().getValue("pvdport");
// 		if (portStr)
// 			pvdPort = atoi(portStr);	
// 	}

	//The normal way to connect to pvd.  PVD needs to be running at the time this function is called.
	//We don't worry about the return value because we are already registered as a listener for connections
	//and thus our onPvdConnected call will take care of setting up our basic connection state.
	physx::debugger::comm::PvdConnection * theConnection = PxVisualDebuggerExt::createConnection(
			getPhysics().getPvdConnectionManager()
		,	pvdHost
		,	pvdPort
		,	4000
		,	theConnectionFlags
	);

	if ( theConnection )
		theConnection->release();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
PhysxWorld::onPvdConnected( physx::debugger::comm::PvdConnection& )
{
	//setup joint visualization.  This gets piped to pvd.
	getPhysics().getVisualDebugger()->setVisualizeConstraints(true);
	//getPhysics().getVisualDebugger()->setVisualDebuggerFlag( PxVisualDebuggerFlags::eTRANSMIT_CONTACTS, true );

	getPhysics().getVisualDebugger()->setVisualDebuggerFlag( PxVisualDebuggerFlags::eTRANSMIT_CONTACTS, true);
	getPhysics().getVisualDebugger()->setVisualDebuggerFlag( PxVisualDebuggerFlags::eTRANSMIT_SCENEQUERIES, true);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
PhysxWorld::onPvdDisconnected( physx::debugger::comm::PvdConnection& )
{
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
PhysxWorld::onPvdSendClassDescriptions( physx::debugger::comm::PvdConnection& )
{
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
